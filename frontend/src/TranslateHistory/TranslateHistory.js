import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import { Table } from 'reactstrap';

class TranslateHistory extends Component {
  render() {
    return (
      <Table>
            <thead>
                <tr>
                    <th>Text</th>
                    <th>Translated Text</th>
                    <th>Language</th>
                    <th>Time</th>
                </tr>
            </thead>
            <tbody>
                {(this.props.items || []).slice(0).reverse().map((item,index) => {
                    return <tr key={"item"+index}>
                                <td>{item.text}</td>
                                <td>{item.translatedText}</td>
                                <td>{item.detectedLanguage}</td>
                                <td><Moment fromNow>{item.date}</Moment></td>
                            </tr>
                })}
            </tbody>
      </Table>
    );
  }
}

TranslateHistory.prototypes = {
    items: PropTypes.arrayOf(
        PropTypes.shape({
            text: PropTypes.string,
            translatedText: PropTypes.string,
            detectedLanguage: PropTypes.string,
            date: PropTypes.date
        }),
    )
}

export default TranslateHistory;