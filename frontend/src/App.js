import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import {Container, Row, Col, Form, FormGroup, Input, Button,
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink, Alert} from "reactstrap";
import TranslateHistory from "./TranslateHistory/TranslateHistory";
import './App.css';

class TranslatePage extends Component {
  state = {
    text: "שלום",
    latestResult: null,
    loading:false,
    error:null
  }

  updateText = (e) => {
    this.setState({text: e.target.value})
  }

  getTranslatedText = (e) => {
    e.preventDefault();
    if (this.state.loading){
      return
    }
    let data = new FormData()
    data.set("text", this.state.text)
    this.setState({loading:true});
    fetch("/api/translate/v1/translatetext/", {
      method: "POST",
      body: data
    }).then(data =>data.json(), err => this.setState({error:"An error occured", loading:false})).then(json => {
      this.setState({latestResult: json, text:"", loading:false, error:null})
      this.props.addNewTranslation(json)
    }, err => this.setState({error:"An error occured", loading:false}))
  }
  render() {
    const {latestResult, text, loading, error} = this.state;
    return (
      <Row className="align-items-center justify-content-center fullscreen">
        <Col xs={12}>
          <Row key="inputform" className="align-items-center justify-content-center">
            <Form onSubmit={this.getTranslatedText}>
              <FormGroup>
                <Input type="text" name="text" value={text} onChange={this.updateText} />
              </FormGroup>
              <Button type="submit" className="btn-block" disabled={loading}>{loading ? "Translating..." : "Translate"}</Button>
            </Form>
          </Row>
          {error && 
            <Row className="text-center justify-content-center" key="error">
            <Col xs={12} md={6} className="py-5">
            <Alert color="danger">
              {error}
            </Alert>
            </Col>
            </Row>
          }
          {latestResult && 
            <Row className="text-center" key="result">
              <Col xs={12} className="py-5">Detected Language: {latestResult.detectedLanguage}</Col>
              <Col xs={12} md={6}>
                <h5>Text:</h5>
                <p>{latestResult.text}</p>
              </Col>
              <Col xs={12} md={6}>
                <h5>Translated Text:</h5>
                <p>{latestResult.translatedText}</p>
              </Col>
            </Row>
          }
        </Col>
      </Row>
    );
  }
}

const TranslateHistoryPage = (props) => <div className="d-flex align-items-center fullscreen"><TranslateHistory items={props.items} /></div>;



class App extends Component {
  state = {
    translatedItems: [{date: "2018-04-23T02:15:37.534Z",
    detectedLanguage: "iw",
    text: "שלום",
    translatedText: "Peace"}]
  }

  addNewTranslation = (item) => {
    let newItems = [...this.state.translatedItems, item];
    this.setState({translatedItems: newItems});
  }

  render() {
    return (
      <Router>
        <div>
          <Navbar color="light" light expand="md">
            <Container>
              <NavbarBrand href="/">Translation</NavbarBrand>
                <Nav className="ml-auto" navbar>
                  <NavItem>
                  <Link to="/"><NavLink>Translate</NavLink></Link>
                  </NavItem>
                  <NavItem>
                    <Link to="/history"><NavLink>History</NavLink></Link>
                  </NavItem>
                </Nav>
              </Container>
            </Navbar>
          <Container>
              <Route exact path="/" render={() => <TranslatePage addNewTranslation={this.addNewTranslation} />} />
              <Route exact path="/history" component={() => <TranslateHistoryPage items={this.state.translatedItems}/>} />
          </Container>
        </div>
      </Router>
    );
  }
}

export default App;
