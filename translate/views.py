from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest
from .models import TranslateRequest
from google.cloud import translate_v2
from .forms import TranslateForm

LANGUAGES = {"ab":"Abkhaz","aa":"Afar","af":"Afrikaans","ak":"Akan","sq":"Albanian","am":"Amharic","ar":"Arabic","an":"Aragonese","hy":"Armenian","as":"Assamese","av":"Avaric","ae":"Avestan","ay":"Aymara","az":"South Azerbaijani","bm":"Bambara","ba":"Bashkir","eu":"Basque","be":"Belarusian","bn":"Bengali; Bangla","bh":"Bihari","bi":"Bislama","bs":"Bosnian","br":"Breton","bg":"Bulgarian","my":"Burmese","ca":"Catalan; Valencian","ch":"Chamorro","ce":"Chechen","ny":"Chichewa; Chewa; Nyanja","zh":"Chinese","cv":"Chuvash","kw":"Cornish","co":"Corsican","cr":"Cree","hr":"Croatian","cs":"Czech","da":"Danish","dv":"Divehi; Dhivehi; Maldivian;","nl":"Dutch","dz":"Dzongkha","en":"English","eo":"Esperanto","et":"Estonian","ee":"Ewe","fo":"Faroese","fj":"Fijian","fi":"Finnish","fr":"French","ff":"Fula; Fulah; Pulaar; Pular","gl":"Galician","ka":"Georgian","de":"German","el":"Greek, Modern","gn":"GuaranÃ­","gu":"Gujarati","ht":"Haitian; Haitian Creole","ha":"Hausa","he":"Hebrew (modern)","hz":"Herero","hi":"Hindi","ho":"Hiri Motu","hu":"Hungarian","ia":"Interlingua","id":"Indonesian","ie":"Interlingue","ga":"Irish","ig":"Igbo","ik":"Inupiaq","io":"Ido","is":"Icelandic","it":"Italian","iu":"Inuktitut","ja":"Japanese","jv":"Javanese","kl":"Kalaallisut, Greenlandic","kn":"Kannada","kr":"Kanuri","ks":"Kashmiri","kk":"Kazakh","km":"Khmer","ki":"Kikuyu, Gikuyu","rw":"Kinyarwanda","ky":"Kyrgyz","kv":"Komi","kg":"Kongo","ko":"Korean","ku":"Kurdish","kj":"Kwanyama, Kuanyama","la":"Latin","lb":"Luxembourgish, Letzeburgesch","lg":"Ganda","li":"Limburgish, Limburgan, Limburger","ln":"Lingala","lo":"Lao","lt":"Lithuanian","lu":"Luba-Katanga","lv":"Latvian","gv":"Manx","mk":"Macedonian","mg":"Malagasy","ms":"Malay","ml":"Malayalam","mt":"Maltese","mi":"MÄori","mr":"Marathi (MarÄá¹­hÄ«)","mh":"Marshallese","mn":"Mongolian","na":"Nauru","nv":"Navajo, Navaho","nb":"Norwegian BokmÃ¥l","nd":"North Ndebele","ne":"Nepali","ng":"Ndonga","nn":"Norwegian Nynorsk","no":"Norwegian","ii":"Nuosu","nr":"South Ndebele","oc":"Occitan","oj":"Ojibwe, Ojibwa","cu":"Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic","om":"Oromo","or":"Oriya","os":"Ossetian, Ossetic","pa":"Panjabi, Punjabi","pi":"PÄli","fa":"Persian (Farsi)","pl":"Polish","ps":"Pashto, Pushto","pt":"Portuguese","qu":"Quechua","rm":"Romansh","rn":"Kirundi","ro":"Romanian, [])","ru":"Russian","sa":"Sanskrit (Saá¹ská¹›ta)","sc":"Sardinian","sd":"Sindhi","se":"Northern Sami","sm":"Samoan","sg":"Sango","sr":"Serbian","gd":"Scottish Gaelic; Gaelic","sn":"Shona","si":"Sinhala, Sinhalese","sk":"Slovak","sl":"Slovene","so":"Somali","st":"Southern Sotho","es":"Spanish; Castilian","su":"Sundanese","sw":"Swahili","ss":"Swati","sv":"Swedish","ta":"Tamil","te":"Telugu","tg":"Tajik","th":"Thai","ti":"Tigrinya","bo":"Tibetan Standard, Tibetan, Central","tk":"Turkmen","tl":"Tagalog","tn":"Tswana","to":"Tonga (Tonga Islands)","tr":"Turkish","ts":"Tsonga","tt":"Tatar","tw":"Twi","ty":"Tahitian","ug":"Uyghur, Uighur","uk":"Ukrainian","ur":"Urdu","uz":"Uzbek","ve":"Venda","vi":"Vietnamese","vo":"VolapÃ¼k","wa":"Walloon","cy":"Welsh","wo":"Wolof","fy":"Western Frisian","xh":"Xhosa","yi":"Yiddish","yo":"Yoruba","za":"Zhuang, Chuang","zu":"Zulu"}


def process_translate(text: str, ip_address: str=None) -> dict:
    if ip_address == None:
        ip_address = "0.0.0.0"
    translate_client = translate_v2.Client()
    result = translate_client.translate(text)
    language = LANGUAGES.get(result['detectedSourceLanguage'], result['detectedSourceLanguage'])
    tr = TranslateRequest(
        entered_text = result['input'],
        detected_language = language,
        translated_text = result['translatedText'],
        ip_address = ip_address
    )
    tr.save()

    return {
        "text": result['input'],
        "detectedLanguage": language,
        "translatedText": result['translatedText'],
        "date": tr.date_created
    }

@csrf_exempt
def translate_string(request) -> HttpResponse:
    if request.method == "POST":
        form = TranslateForm(request.POST)
        if form.is_valid():
            response_dict = process_translate(form.cleaned_data["text"], ip_address = request.META["REMOTE_ADDR"])

            return JsonResponse(response_dict)
    return HttpResponseBadRequest()


        