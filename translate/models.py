from django.db import models

# Create your models here.

class TranslateRequest(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    entered_text = models.TextField()
    detected_language = models.CharField(max_length=35)
    translated_text = models.TextField()
    ip_address = models.GenericIPAddressField()
