from django.contrib import admin
from .models import TranslateRequest
# Register your models here.

class TranslateRequestAdmin(admin.ModelAdmin):
    list_display = ["id","entered_text", "translated_text", "detected_language", "date_created"]
    list_filter = ["detected_language",]
    class Meta:
        model = TranslateRequest


admin.site.register(TranslateRequest, TranslateRequestAdmin)